package com.avoka;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * Assumptions
 *      - This code is available as a 'maven' project in bitbucket git repository at the URL given below:
 *              https://bitbucket.org/techtrekkies/unitconverter.git
 *      Design::
 *      - Used Java 8 to Leverage on language features.
 *      - Conversion Logic is extensible/customizable to wide variety of conversion requirements.
 *      - Use Generics and Lambdas to ensure extensibility/customizability and ease of use.
 *      - Segragation of Responsbilities - Beteween Converter,Units,Conversion Function and so on...
 *      - ConversionTemplate extendable for Different Conversion Type
 *          - TemperatureConverter is the concrete converter for Temperature Conversion and the main class
 *      - Uses Fluent Interface as Requested for 'Ease of Use'
 *
 *      Reqs::
 *      - Defaults to 2 Decimal Place (Provides API to change this)
 *      - Sanity Checking for most use cases is done -
 *          - Throw exception if 'required' fields aren't supplied.
 *          - Throw exception if conversion types aren't supported (for ex., Celsius to Meter etc).
 *      - Per reuqirement to provide as a single '.txt' file the entire code is put in single 'java' file.
 *
 *      Other:
 *      - public static void main (PSVM) for demonstration purposes only.
 *      - exception print stack trace for demonstration purposes only.
 *      - No tests provided here (Because this is asked as a single '.txt' file) but using PSVM instead.
 */
public class TemperatureConverter extends ConversionTemplate<Double, Double> {

    /**
     * Using a base unit approach instead of maintaing a large suites of formulas...
     * Initialize conversion base unit type and conversion formula for to/from to different unit types
     */
    @Override
    protected void initialize() {
        conversionType = ConversionType.Temparature;
        baseUnitType = TemperatureUnitType.Celsius;
        formulaMap = new HashMap<>();

        // Celsius
        formulaMap.put(TemperatureUnitType.Celsius,
                new ConversionMatrix<Double, Double>((t) -> (t * 1), (t) -> (t * 1)));

        // Celsius to Fahrenheit
        formulaMap.put(TemperatureUnitType.Fahrenheit,
                new ConversionMatrix<Double, Double>((t) -> (t * 1.8) + 32, (t) -> (t - 32) * 0.5556));

        // Celsius to Kelvin
        formulaMap.put(TemperatureUnitType.Kelvin,
                new ConversionMatrix<Double, Double>((t) -> (t + 273.15), (t) -> (t - 273.15)));
    }

    public Double convert() {
        if (conversionValue==null) { throw new IllegalArgumentException("Value for conversion must be supplied."); }
        if (fromUnitType==null) { throw new IllegalArgumentException("From conversion unit must be supplied."); }
        if (toUnitType==null) { throw new IllegalArgumentException("To conversion unit must be supplied."); }

        if (fromUnitType.equals(toUnitType)) return conversionValue;
        ConversionMatrix<Double, Double> matrix=null;
        if (fromUnitType.equals(baseUnitType)) {
            matrix = formulaMap.get(toUnitType);
            if (matrix==null) { throw new UnsupportedOperationException("To conversion operation is unsupported"); }
            toValue = matrix.getToTargetType().compute(conversionValue);
        } else {
            matrix = formulaMap.get(fromUnitType);
            if (matrix==null) { throw new UnsupportedOperationException("From conversion operation is unsupported"); }
            Double baseValue = matrix.getToBaseType().compute(conversionValue);

            matrix = formulaMap.get(toUnitType);
            if (matrix==null) { throw new UnsupportedOperationException("From conversion operation is unsupported"); }
            toValue = matrix.getToTargetType().compute(baseValue);
        }

        // Return converted value with 'defaultPrecision'
        return new BigDecimal(toValue.toString()).setScale(defaultPrecision, RoundingMode.HALF_UP).doubleValue();
    }

    public static void main(String[] args) {

        System.out.println(
                new TemperatureConverter()
                        .from(100.55d, TemperatureUnitType.Celsius)
                        .to(TemperatureUnitType.Celsius)
                        .convert());

        System.out.println(
                new TemperatureConverter()
                    .from(100.75d, TemperatureUnitType.Celsius)
                    .to(TemperatureUnitType.Fahrenheit)
                    .convert());

        System.out.println(
                new TemperatureConverter()
                        .from(100d, TemperatureUnitType.Fahrenheit)
                        .to(TemperatureUnitType.Kelvin)
                        .convert());

        System.out.println(
                new TemperatureConverter()
                        .from(100d, TemperatureUnitType.Kelvin)
                        .to(TemperatureUnitType.Celsius)
                        .convert());

        System.out.println(
                new TemperatureConverter()
                        .from(100d, TemperatureUnitType.Kelvin)
                        .to(TemperatureUnitType.Kelvin)
                        .convert());

        // Change precision example - demonstration option only...
        System.out.println(
                new TemperatureConverter()
                        .from(100d, TemperatureUnitType.Fahrenheit)
                        .to(TemperatureUnitType.Kelvin)
                        .changePrecision((short) 3)
                        .convert());

        // Demonstration Purposes Only

//        // Missing To Unit Type
//        try {
//            System.out.println(
//                    new TemperatureConverter()
//                            .from(100d, TemperatureUnitType.Kelvin)
//                            .convert());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // Wrong - To Unit Type
//        try {
//            System.out.println(
//                    new TemperatureConverter()
//                            .from(100d, TemperatureUnitType.Kelvin)
//                            .to(LengthUnitType.Meter)
//                            .convert());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        // Temperature Tables - Celsius to Fahrenheit/Kelvin
        TemperatureConverter tc = new TemperatureConverter();
        tc.fromUnitType(TemperatureUnitType.Celsius);
        System.out.println("SNo.\tCelsius*\tFahrenheit\tKelvin");
        for (int i=1;i<=10;i++) {
            System.out.print("\t\t" + 10d * i);
            System.out.print("\t" + tc.from(10d * i).to(TemperatureUnitType.Fahrenheit).convert());
            System.out.println("\t" + tc.from(10d * i).to(TemperatureUnitType.Kelvin).convert());
        }

        // Temperature Tables - Fahrenheit to Celsius/Kelvin
        tc.fromUnitType(TemperatureUnitType.Fahrenheit);
        System.out.println("SNo.\tCelsius\tFahrenheit*\ttKelvin");
        for (int i=1;i<=10;i++) {
            System.out.print("\t\t" + tc.from(10d * i).to(TemperatureUnitType.Celsius).convert());
            System.out.print("\t" + 10d * i);
            System.out.println("\t" + tc.from(10d * i).to(TemperatureUnitType.Kelvin).convert());
        }

        // Temperature Tables - Kelvin to Celsius/Fahrenheit
        tc.fromUnitType(TemperatureUnitType.Kelvin);
        System.out.println("SNo.\tCelsius\tFahrenheit\tKelvin*");
        for (int i=1;i<=10;i++) {
            System.out.print("\t\t" + tc.from(10d * i).to(TemperatureUnitType.Celsius).convert());
            System.out.print("\t" + tc.from(10d * i).to(TemperatureUnitType.Fahrenheit).convert());
            System.out.println("\t" + 10d * i);
        }
    }
}

abstract class ConversionTemplate<FromValueType, ToValueType> {
    protected IConversionType conversionType;
    protected IUnitType baseUnitType;
    protected Map<IUnitType, ConversionMatrix<FromValueType, ToValueType>> formulaMap;
    protected IUnitType fromUnitType;
    protected IUnitType toUnitType;
    protected short defaultPrecision = 2;
    protected FromValueType conversionValue;
    protected ToValueType toValue;

    /**
     *
     */
    public ConversionTemplate() {
        initialize();
    }

    protected abstract void initialize();

    public abstract ToValueType convert() ;

    public ConversionTemplate<FromValueType, ToValueType> fromUnitType(IUnitType fromUnitType) {
        this.fromUnitType = fromUnitType;
        return this;
    }

    public ConversionTemplate<FromValueType, ToValueType> from(FromValueType fromValue) {
        this.conversionValue = fromValue;
        return this;
    }

    public ConversionTemplate<FromValueType, ToValueType> from(FromValueType fromValue, IUnitType fromUnitType) {
        return this.fromUnitType(fromUnitType).from(fromValue);
    }

    public ConversionTemplate<FromValueType, ToValueType> to(IUnitType toUnitType) {
        this.toUnitType = toUnitType;
        return this;
    }

    public ConversionTemplate<FromValueType, ToValueType> changePrecision(short newPrecision) {
        this.defaultPrecision = newPrecision;
        return this;
    }

}

/**
 * A utility class to encapsulate From / To forumalae that uses lambda to delegate implementation
 * @param <FromType>
 * @param <ToType>
 */
class ConversionMatrix<FromType, ToType> {
    private FormulaFunction<FromType, ToType> toTargetType;
    private FormulaFunction<FromType, ToType> toBaseType;

    public ConversionMatrix(FormulaFunction<FromType, ToType> toTargetType, FormulaFunction<FromType, ToType> toBaseType) {
        this.toTargetType = toTargetType;
        this.toBaseType = toBaseType;
    }

    public FormulaFunction<FromType, ToType> getToTargetType() {
        return toTargetType;
    }

    public void setToTargetType(FormulaFunction<FromType, ToType> toTargetType) {
        this.toTargetType = toTargetType;
    }

    public FormulaFunction<FromType, ToType> getToBaseType() {
        return toBaseType;
    }

    public void setToBaseType(FormulaFunction<FromType, ToType> toBaseType) {
        this.toBaseType = toBaseType;
    }
}

@FunctionalInterface
interface FormulaFunction<FromType, ToType> {
    ToType compute(FromType v);
}

interface IConversionType {
}

interface IUnitType {
}

/**
 *
 */
enum ConversionType implements IConversionType {
    Temparature(TemperatureUnitType.values()),
    Length(null);

    ConversionType(IUnitType[] unitTypes) {
        this.unitTypes = unitTypes;
    }

    private IUnitType[] unitTypes;
}

enum TemperatureUnitType implements IUnitType {
    Celsius,
    Fahrenheit,
    Kelvin;
}

enum LengthUnitType implements IUnitType {
    Meter,
    Centimeter,
    Millimeter;
}